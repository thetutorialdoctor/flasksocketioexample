from flask import Flask,render_template
from flask_socketio import SocketIO, rooms,emit,join_room,leave_room
app = Flask(__name__)
socketio = SocketIO(cors_allowed_origins="*")
socketio.init_app(app)


@app.route('/')
def chat():
    return render_template('chat1.html')    

@app.route('/kamiboo')
def chat2():
    return render_template('chat2.html')    

@socketio.on("connect")
def on_connect(auth):
    print(auth)
    if auth.get('token') != '123':
        raise ConnectionRefusedError('Unauthorized')

@socketio.on('message')
def handle_message(data):
    username = data.get('username')
    message = data.get('message')
    room = data.get('room') #grouping of where connections exist
    if room in rooms():
        emit('message', {"username": username, "message": message}, to=room)

@socketio.on('join')
def handle_join(data):
    username = data.get('username')
    room = data.get('room') #grouping of where connections exist
    join_room(room)
    emit('join', {"username": username}, to=room)

@socketio.on('leave')
def handle_leave(data):
    username = data.get('username')
    room = data.get('room') #grouping of where connections exist
    leave_room(room)
    emit('leave', {"username": username}, to=room)

if __name__ == "__main__":
    socketio.run(app,port=4000,debug=True)
